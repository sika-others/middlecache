from middlecache import get_wsgi
from middlecache.backends import RedisBackend

cache_backend = RedisBackend()

application = get_wsgi(cache_backend=cache_backend, timeout=3)

if __name__ == '__main__':
    from werkzeug.serving import run_simple
    from werkzeug.debug import DebuggedApplication

    application = DebuggedApplication(application, evalex=True)
    run_simple('localhost', 4000, application)
