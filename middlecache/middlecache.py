import requests

from werkzeug.wrappers import Request, Response


def get_wsgi(cache_backend, timeout=3):
    @Request.application
    def application(request):
        url = u'%s?%s' % (request.path[1:], request.query_string)
        print url
        data = cache_backend.get(url)
        if data != None:
            return Response(data)
        try:
            data = requests.get(url, timeout=timeout).text
        except:
            return Response('', status=500)
        cache_backend.set(url, data)
        return Response(data)
    return application

