#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

setup(
    name = 'middlecache',
    version = '0.1.0',
    download = 'http://github.com/sikaondrej/middlecache',
    license = 'MIT',
    description = 'Simple cache',
    author = 'Ondrej Sika',
    author_email = 'ondrej@ondrejsika.com',
    packages = find_packages(),
    install_requires = ['werkzeug', 'requests', ],
)

